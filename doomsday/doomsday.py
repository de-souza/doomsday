from doomsday.utils import anchor, key, leap, weekday


def doomsday (enter):
	if len(enter) < 8 :
		return("Bonvolu uzi la ISO 8601 formaton: (\"YYYY-MM-DD\")")
	elif enter[-6] != '-' or enter[-3] != '-' :
		return("Bonvolu uzi la ISO 8601 formaton: (\"YYYY-MM-DD\")")

	YYYY = int(enter[:-6])
	if YYYY < 1593 :
		print("Gregoria kalendaro ne ekistis antaŭ ĉi tiu date…")

	if enter[-5] == 0:
		MM = int(enter[-4:-3])
	else :
		MM = int(enter[-5:-3])
	if MM < 0 or MM > 12 :
		return ("Ne!")

	if enter[-2] == 0:
		DD = int(enter[-1:])
	else :
		DD = int(enter[-2:])
	if DD < 0 or DD > 31 :
		return ("Ne!")
	elif (MM == 4 or MM == 6 or MM == 9 or MM == 11 ) and DD > 30 :
		return ("Ne!")
	elif ( MM == 2 ) :
		if   DD > 29 :
				return ("Ne!")
		elif leap(YYYY) == False and DD > 28:
				return ("Ne!")

	return (weekday(anchor(YYYY)-key(YYYY,MM)+DD))
