# Doomsday

## Prérequis

- [Python 3](https://www.python.org/)

## Exécution

1. Cloner le dépôt

       git clone https://gitlab.com/esteka3000/doomsday.git

2. Naviguer dans le dossier

       cd doomsday

3. Lancer le script

       python doomsday.py

## Licence

Ce projet est distribué selon les termes de la licence [GNU General Public License v3.0](gitlab.com/esteka3000/doomsday/blob/master/LICENSE).
